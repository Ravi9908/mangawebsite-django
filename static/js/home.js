document.addEventListener("DOMContentLoaded", () => {
  const menuBtn = document.querySelector(".menu-btn");
  const navbar = document.querySelector(".navbar");
  const closeBtn = document.querySelector(".close-btn");

  menuBtn.addEventListener("click", () => {
    menuBtn.style.display = "none";
    navbar.style.display = "block";
  });
  closeBtn.addEventListener("click", () => {
    navbar.style.display = "none";
    menuBtn.style.display = "block";
  });

  setTimeout(() => {
    const messagesDiv = document.querySelector(".messages");
    if (messagesDiv) {
      messagesDiv.classList.add("fade-out");
      setTimeout(() => {
        messagesDiv.style.display = "none";
      }, 500);
    }
  }, 3000);

  const modal = document.getElementById("commentModal");
  const buttons = document.getElementsByClassName("open-modal");
  const span = document.getElementsByClassName("close")[0];
  const form = document.getElementById("commentForm");

  let ratingId;

  for (let i = 0; i < buttons.length; i++) {
    buttons[i].onclick = function () {
      ratingId = this.getAttribute("data-rating-id");
      console.log(ratingId)
      form.action = form.getAttribute("data-base-url").replace("0", ratingId);
      modal.style.display = "block";
    };
  }

  span.onclick = function () {
    modal.style.display = "none";
  };

  window.onclick = function (event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  };
  document.querySelectorAll(".like-form").forEach((form) => {
    form.addEventListener("submit", function (event) {
      event.preventDefault();

      const ratingId =
        this.querySelector(".like-button").getAttribute("data-rating-id");
      const csrfToken = document.querySelector(
        "[name=csrfmiddlewaretoken]"
      ).value;
      if (!ratingId) {
        console.error("Rating ID is missing");
        return;
      }

      fetch(`/post/like/${ratingId}/`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": csrfToken,
        },
        body: JSON.stringify({}),
      })
        .then((response) => {
          if (response.redirected) {
            window.location.href = response.url; 
            return;
          }
          return response.json();
        })
        .then((data) => {
          if (data.error) {
            console.error(data.error);
          } else {
            const likeButton = this.querySelector(".like-button");
            const likesCount = document.getElementById(`like-count-${ratingId}`);
            if (data.liked) {
              likeButton.classList.add("liked");
              likeButton.textContent = "Unlike";
            } else {
              likeButton.classList.remove("liked");
              likeButton.textContent = "Like";
            }
            likesCount.textContent = data.likes_count; 
          }
        })
        .catch((error) => console.error("Error:", error));
    });
  });
});

function toggleNestedComments(ratingId) {
  const nestedComments = document.getElementById("nested-comments-" + ratingId);
  const showButton = document.getElementById("show-more-button-" + ratingId);
  if (
    nestedComments.style.display === "none" ||
    nestedComments.style.display === ""
  ) {
    nestedComments.style.display = "block";
    showButton.textContent = "Show less";
  } else {
    nestedComments.style.display = "none";
    showButton.textContent = "Show Comments";
  }
}
