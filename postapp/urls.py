from django.urls import path
from postapp import views



urlpatterns = [
    path('post_view/',views.post_view, name='posts'),
    path('edit_post/<int:post_id>/', views.edit_post, name='edit'),
    path('manga/<int:post_id>/', views.manga_detail, name='post_detail'),
    path('reviews/<int:rating_id>/comment/', views.add_comment_to_review, name='add_comment_to_review'),
    path("like/<int:rating_id>/", views.like_view, name='like_post'),
    path('genre/<str:genre>/', views.genre_posts, name='genre_posts'),
    path('follow/<str:username>/', views.follow, name='follow'),
    path('send_friend_request/<str:username>/', views.send_friend_request, name='send_friend_request'),
    path('accept_friend_request/<int:request_id>/', views.accept_friend_request, name='accept_friend_request'),
    path('cancel-friend-request/<int:request_id>/', views.cancel_friend_request, name='cancel_friend_request'),
    path('reject_friend_request/<int:request_id>/', views.reject_friend_request, name='reject_friend_request'),
    path('search/', views.search_view, name='search'),
]