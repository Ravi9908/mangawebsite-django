from django.contrib import admin
from postapp.models import Post,Rating,ReviewComment,FriendRequest
from .models import User

# Register your models here.


admin.site.register(Post)
admin.site.register(Rating)
admin.site.register(ReviewComment)
admin.site.register(FriendRequest)
