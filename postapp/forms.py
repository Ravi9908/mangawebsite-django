from django import forms
from postapp.models import Post,Rating,ReviewComment


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title','photo','genre','author','description']

class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ['rating','review']

class ReviewCommentForm(forms.ModelForm):
    class Meta:
        model = ReviewComment
        fields = ['comment']


