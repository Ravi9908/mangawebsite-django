from django.shortcuts import render, redirect, get_object_or_404
from postapp.forms import PostForm, RatingForm, ReviewCommentForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from postapp.models import Post, Rating, ReviewComment, FriendRequest, Follow
from django.db.models import Avg, Count
from django.urls import reverse
from django.contrib.auth.models import User
from django.http import JsonResponse

# Create your views here.


@login_required(login_url='/login/')
def post_view(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()
            messages.success(request, 'Post created successfully')
            return redirect('posts')
        else:
            print(form.errors)
            messages.error(request, 'Error creating post')
    else:
        form = PostForm()
    return render(request, 'postapp/post.html', {'form': form})


@login_required(login_url='/login/')
def edit_post(request, post_id):
    post = get_object_or_404(Post, id=post_id, user=request.user)
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES, instance=post)
        if form.is_valid():
            form.save()
            username = request.user.username
            return redirect('profile',username)
    else:
        form = PostForm(instance=post)
    return render(request, 'postapp/edit_post.html', {'form': form})


def manga_detail(request, post_id):
    post = get_object_or_404(Post, id=post_id)
    url = request.META.get("HTTP_REFERER")
    if request.method == 'POST':
        try:
            reviews = Rating.objects.get(
                user__id=request.user.id, post__id=post_id)
            form = RatingForm(request.POST, instance=reviews)
            form.save()
            messages.success(request, 'your review has been updated')
            return redirect(url)
        except Rating.DoesNotExist:
            form = RatingForm(request.POST)
            if form.is_valid():
                rating = form.save(commit=False)
                rating.post = post
                rating.user = request.user
                rating.save()
                messages.success(request, 'your review has been submitted')
                return redirect(url)
            else:
                messages.error(request, 'Error submitting review')
    rating_count = Rating.objects.filter(post=post).count()
    rating_data = {i: 0 for i in range(1, 6)}
    rating_distribution = Rating.objects.filter(post=post).values(
        'rating').annotate(count=Count('rating')).order_by('-rating')
    for item in rating_distribution:
        rating_data[item['rating']] = item['count']
    avg_rating = Rating.objects.filter(post=post).aggregate(Avg('rating'))[
        'rating__avg'] or 0
    comments = Rating.objects.filter(post=post).select_related(
        'user').prefetch_related('comments__user').order_by('-timestamp')
    reviews_count = Rating.objects.filter(post=post).count()
    nested_comments_count = {}
    for rating in Rating.objects.filter(post=post):
        comments_count = ReviewComment.objects.filter(rating=rating).count()
        nested_comments_count[rating.id] = comments_count
    likes_count = {rating.id: rating.likes.count() for rating in comments}
    return render(request, 'postapp/post_detail.html', {
        'post': post,
        'avg_rating': avg_rating,
        'rating_data': rating_data,
        'rating_count': rating_count,
        'comments': comments,
        'reviews_count': reviews_count,
        'nested_comments_count': nested_comments_count,
        'likes_count': likes_count,
    })


@login_required(login_url='/login/')
def add_comment_to_review(request, rating_id):
    rating = get_object_or_404(Rating, id=rating_id)
    if request.method == 'POST':
        form = ReviewCommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.rating = rating
            comment.save()
            return redirect('post_detail', post_id=rating.post.id)
    else:
        form = ReviewCommentForm()
    return render(request, 'post_detail.html', {'form': form, 'rating': rating})


@login_required(login_url='/login/')
def like_view(request, rating_id):
    review = get_object_or_404(Rating, id=rating_id)
    if request.user in review.likes.all():
        review.likes.remove(request.user)
        liked = False
    else:
        review.likes.add(request.user)
        liked = True
    response_data = {
        'liked': liked,
        'likes_count': review.likes.count(),
    }
    return JsonResponse(response_data, safe=False)


@login_required(login_url='/login/')
def follow(request, username):
    user = request.user
    follower = get_object_or_404(User, username=username)
    if user != follower:
        follow_instance, created = Follow.objects.get_or_create(
            user=follower, follower=user)
        if not created:
            follow_instance.delete()
    return redirect('profile', username=username)



@login_required(login_url='/login/')
def send_friend_request(request, username):
    to_user = get_object_or_404(User, username=username)
    friend_request, created = FriendRequest.objects.get_or_create(
        from_user=request.user, to_user=to_user)
    if created:
        pass
    return redirect('profile', username=username)


@login_required(login_url='/login/')
def accept_friend_request(request, request_id):
    friend_request = FriendRequest.objects.get(pk=request_id)

    if friend_request.to_user != request.user:
        return redirect('friend_requests')
    friend_request.status = 'ACCEPTED'
    friend_request.save()
    messages.success(request, 'Friend request accepted')
    return redirect('profile', username=friend_request.from_user.username)


@login_required(login_url='/login/')
def cancel_friend_request(request, request_id):
    friend_request = get_object_or_404(FriendRequest, pk=request_id)

    if friend_request.from_user != request.user:
        return redirect('profile', username=request.user.username)

    friend_request.delete()

    return redirect('profile', username=friend_request.to_user.username)


@login_required(login_url='/login/')
def reject_friend_request(request, request_id):
    friend_request = FriendRequest.objects.get(pk=request_id)

    if friend_request.to_user != request.user:
        messages.success('friend request sent success')
    friend_request.status = 'REJECTED'
    friend_request.save()
    messages.success(request, 'Friend request rejected')
    return redirect('profile', username=friend_request.from_user.username)


def genre_posts(request, genre):
    posts = Post.objects.filter(genre__icontains=genre)
    return render(request, 'postapp/genre_posts.html', {'posts': posts, 'genre': genre})

def search_view(request):
    posts = ''
    if request.method == 'POST':
        searched = request.POST.get('search', '')
        if searched:
            posts = Post.objects.filter(title__icontains = searched)
    return render(request, 'postapp/genre_posts.html', {'posts': posts})

    
