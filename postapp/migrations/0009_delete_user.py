# Generated by Django 5.0.6 on 2024-06-13 04:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('postapp', '0008_user'),
    ]

    operations = [
        migrations.DeleteModel(
            name='User',
        ),
    ]
