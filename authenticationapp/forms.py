from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from authenticationapp.models import ProfileUser
import os


class signUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name',
                  'email', 'password1', 'password2']


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name']


def get_profile_picture_path(instance, filename):
    return os.path.join('profile_pictures', filename)


class ProfileUserForm(forms.ModelForm):
    profile_picture = forms.ImageField(widget=forms.FileInput)

    class Meta:
        model = ProfileUser
        fields = ['profile_picture', 'gender']
