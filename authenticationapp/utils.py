from postapp.models import FriendRequest
def get_friends(user):
    sent_friend_requests = FriendRequest.objects.filter(from_user=user, status='ACCEPTED')
    received_friend_requests = FriendRequest.objects.filter(to_user=user, status='ACCEPTED')
    
    friends = set()
    for request in sent_friend_requests:
        friends.add(request.to_user)
    for request in received_friend_requests:
        friends.add(request.from_user)
    
    return friends