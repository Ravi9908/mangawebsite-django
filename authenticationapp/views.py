from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth import login, logout, authenticate, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from authenticationapp.forms import signUpForm, UserProfileForm, ProfileUserForm
from django.contrib import messages
from postapp.models import Post,Follow,FriendRequest
from django.contrib.auth.models import User
from authenticationapp.models import ProfileUser
from authenticationapp.utils import get_friends



# Create your views here.


def sign_up(request):
    form = signUpForm()
    if request.method == 'POST':
        form = signUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            ProfileUser.objects.create(user=user)
            messages.success(request, 'Account created successfully')
            form.save()
            return redirect('login')
    return render(request, 'authentication/signup.html', {'form': form})


def log_in(request):
    form = AuthenticationForm()
    if request.method == 'POST':
        form = AuthenticationForm(request, request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.error(request, 'Invalid username or password.')
    return render(request, 'authentication/login.html', {'form': form})


@login_required
def profile_edit(request):
    user = request.user
    try:
        profile_instance = user.profileuser
    except ProfileUser.DoesNotExist:
        profile_instance = ProfileUser(user=user)
        profile_instance.save()
    if request.method == 'POST':
        profile_form = ProfileUserForm(
            request.POST, request.FILES, instance=profile_instance)
        user_form = UserProfileForm(request.POST, instance=user)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Profile updated successfully')
            return redirect('profile_edit')
    else:
        user_form = UserProfileForm(instance=user)
        profile_form = ProfileUserForm(instance=profile_instance)

    return render(request, 'authentication/profile_edit.html', {"user_form": user_form, "profile_form": profile_form})




@login_required(login_url='/login/')
def profile(request, username):
    profile_user = get_object_or_404(User, username=username)
    followers_count = Follow.objects.filter(user=profile_user).count()
    following_count = Follow.objects.filter(follower=profile_user).count()
    profile_details = get_object_or_404(ProfileUser, user=profile_user)
    images = Post.objects.filter(user=profile_user)
    is_following = Follow.objects.filter(user=profile_user, follower=request.user).exists()
    friend_request_sent = FriendRequest.objects.filter(from_user=request.user, to_user=profile_user, status='PENDING').first()
    friend_request_received = request.user.received_requests.filter(status='PENDING').first()
    is_own_profile = request.user == profile_user
    friends = get_friends(profile_user)
    are_friends = profile_user in get_friends(request.user)
    return render(request, 'authentication/profile.html', {
        'profile_user': profile_user,
        'followers_count': followers_count,
        'following_count': following_count,
        'profile_details': profile_details,
        'images': images,
        'is_following': is_following,
        'friend_request_sent': friend_request_sent,
        'friend_request_received': friend_request_received,
        'is_own_profile': is_own_profile,
        'friends':friends,
        'are_friends':are_friends
    })

def home(request):
    images = Post.objects.all().order_by('-timestamp')
    genres = [
        "Action",
        "Adventure",
        "Comedy",
        "Drama",
        "Fantasy",
        "Historical",
        "Horror",
        "Mystery",
        "Romance",
        "Sci-Fi",
        "Slice of Life",
        "Sports",
        "Supernatural",
        "Thriller",
        "Psychological",
        "Mecha",
        "School",
        "Magic",
        "Music",
        "Parody",
        "Space",
        "Super Power",
        "Military",
        "Cyberpunk",
        "Game",
        "Seinen",
        "Shoujo",
        "Shounen",
        "Josei",
        "Harem",
        "Ecchi",
        "Isekai",
        "Vampire",
        "Demons",
        "Police",
        "Samurai",
        "Historical",
        "Post-Apocalyptic",
        "Martial Arts",
        "Mystery",
        "Psychological",
        "Supernatural",
        "Thriller",
        "Zombies",
        "Dystopia",
        "Medical",
    ]

    return render(request, 'authentication/index.html', {"images": images,'genres':genres})


def log_out(request):
    logout(request)
    return redirect('login')

@login_required(login_url='/login/')
def change_password_with_old_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(
                request, 'Your password was successfully updated!')
            return redirect('profile_edit')
        else:
            messages.error(request, 'Please enter correct password')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'authentication/changepassword.html', {'form': form})


@login_required(login_url='/login/')
def delete_post(request, post_id):
    post = get_object_or_404(Post, id=post_id, user=request.user)
    post.delete()
    return redirect('profile',request.user.username)


