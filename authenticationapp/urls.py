from django.urls import path
from authenticationapp import views


urlpatterns = [
    path('signup/', views.sign_up,name='signup'),
    path('login/',views.log_in, name='login'),
    path('profile/<str:username>/', views.profile, name='profile'),
    path('profile_edit/', views.profile_edit, name='profile_edit'),
    path('logout/', views.log_out, name='logout'),
    path('changepass/', views.change_password_with_old_password,name='changepassword'),
    path('',views.home, name='home'),
    path('delete_post/<int:post_id>/', views.delete_post, name='delete'),
]